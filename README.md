# README #

This project is a sample of POM framework in action. The sample data is obtained and populated against the fields to run the selenium application against the site http://sydneytesters.herokuapp.com/

### What is this repository for? ###

* Quick summary
There are some prerequisites for running the application. This project is a mavenized project. I chose maven since the dependencies are automatically downloaded and the project is easy to manage, run and deploy and start.
### Prerequisites :- ###
1. JAVA - JDK 1.8
2. Maven 3

* Version
0.0.1-SNAPSHOT

### How do I directly run the binary? ###
* Deployment instructions:
The project can be run directly if you wish to see the code in action. 
(There is a maven task defined to automatically create the project binary with all the dependencies)
**Just download the complete dist/ folder** containing the jar with dependencies and the chromedriver.exe
Run the following command (Please make sure Java version is JDK 8)
* java -jar getquote-0.0.1-SNAPSHOT-jar-with-dependencies.jar

### How do I get set up? ###

* Summary of set up:
Following  steps will allow the user to download the project and build the maven project.
The project dependencies will automatically be downloaded.
In addition, the project has been configured with an exec task which will run the test suite.

1. git clone https://username@bitbucket.org/AnanyaShetty/cadence.git
2. cd Cadence/getquote
3. mvn clean install
4. mvn exec:java  (There is a maven task defined which will run the project binary  automatically)

* Dependencies
JDK 1.8


### Who do I talk to? ###
* Repo owner or admin
Ananya Shetty
ananya001@gmail.com