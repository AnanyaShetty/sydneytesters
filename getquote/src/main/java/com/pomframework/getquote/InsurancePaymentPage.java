package com.pomframework.getquote;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InsurancePaymentPage {
	
	WebDriver d =null;
	
	@FindBy(id="cardholdername")
	WebElement cardname;
	
	@FindBy(xpath="html/body/div/div[3]/div[2]/div/div/div/div/div[2]/form/fieldset/div[2]/div/input")
	WebElement email;
	
	@FindBy(xpath="html/body/div/div[3]/div[2]/div/div/div/div/div[2]/form/fieldset/div[3]/div/input")
	WebElement password;
	
	@FindBy(id="card-number")
	WebElement cardno;
	
	@FindBy(id="expiry-month")
	WebElement month;
	
	@FindBy(xpath="//*[@id='ccForm']/fieldset/div[5]/div/div/div[2]/select")
	WebElement year;
	
	@FindBy(id="cvv")
	WebElement cvv;
	
	@FindBy(xpath="//*[@id='pay']")
	WebElement pay;
	
	public InsurancePaymentPage(WebDriver d){
		this.d=d;
		PageFactory.initElements(this.d, this);
	}
	
	
	public void cardname(){
		cardname.sendKeys("XYZY");
	}
	
	public void Email(){
		email.sendKeys("sample@gmail.com");
	}
	
	public void password(){
		password.sendKeys("XYZ");
	}

	public void cardno(){
		cardno.sendKeys("4200000000000000");
	}
	
	public void month(){
		Select m = new Select(month);
		m.selectByIndex(7);
	}
	
	public void year(){
		Select m = new Select(year);
		m.selectByIndex(8);
	}
	
	public void cvv(){
		cvv.sendKeys("345");
	}
	
	public void pay(){
		pay.click();
	}
	
	public void InsurancePayment(){
		this.cardname();
		this.Email();
		this.password();
		this.cardno();
		this.month();
		this.year();
		this.cvv();
		this.pay();
	}
	
}
