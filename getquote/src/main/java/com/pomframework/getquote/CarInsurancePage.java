package com.pomframework.getquote;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CarInsurancePage {

	WebDriver d=null;
	//Create WebElement Locators
	@FindBy(xpath="//*[@id='make']")
	WebElement make;
	
	@FindBy(xpath="//*[@id='year']")
	WebElement year;
	
	@FindBy(xpath="//*[@id='age']")
	WebElement age;
	
	@FindBy(xpath="//*[@id='state']")
	WebElement state;
	
	@FindBy(id="email")
	WebElement email;
	
	@FindBy(xpath="//*[@id='getquote']")
	WebElement getquote;
	
	//Create constructor
	public CarInsurancePage(WebDriver d){
		this.d=d;
		PageFactory.initElements(this.d, this);
	}
	
	//Methods for user action of each Web Elements
	public void make(){
		Select m = new Select(make);
		m.selectByVisibleText("BMW");
	}
	
	public void year(){
		year.sendKeys("2014");
	}
	
	public void age(){
		age.sendKeys("30");
	}
	
	public void state(){
		Select s = new Select(state);
		s.selectByIndex(2);
	}
	
	public void Email(){
		email.sendKeys("sample@gmail.com");
	}
	
	public void getquote(){
		getquote.click();
	}
	
	//All Methods
	public void GetCarQuote() throws Throwable{
		this.make();
		this.year();
		this.age();
		this.state();
		this.Email();
		Thread.sleep(2000);
		this.getquote();
	}
	
}
