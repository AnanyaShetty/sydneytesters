package com.pomframework.getquote;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class QuickQuoteTest {

	public static void main(String[] args) throws Throwable {
		
		WebDriver d=Util.OpenBrowser();
		d.get("http://sydneytesters.herokuapp.com/");

		d.findElement(By.xpath("//*[@id='getcarquote']")).click();
		
        CarInsurancePage car = new CarInsurancePage(d);
        car.GetCarQuote();
        
        BuyInsurancePage buy = new BuyInsurancePage(d);
        buy.BuyInsurance();
        
        InsurancePaymentPage pay= new InsurancePaymentPage(d);
        pay.InsurancePayment();
        
        Thread.sleep(1000);
        d.close();
	}

}
