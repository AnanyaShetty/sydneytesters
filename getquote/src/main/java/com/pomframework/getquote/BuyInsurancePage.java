package com.pomframework.getquote;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuyInsurancePage {

	WebDriver d=null;
	
	@FindBy(xpath="//*[@id='payment']")
	WebElement buy;
	
	public BuyInsurancePage(WebDriver d){
		this.d=d;
		PageFactory.initElements(this.d, this);
	}
	
	public void Buy(){
		buy.click();
	}
	
	public void BuyInsurance(){
		this.Buy();
	}
}
